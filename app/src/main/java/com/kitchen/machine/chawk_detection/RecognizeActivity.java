package com.kitchen.machine.chawk_detection;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import org.tensorflow.contrib.android.TensorFlowInferenceInterface;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.locks.ReentrantLock;

public class RecognizeActivity extends Activity {

    private static final int SAMPLE_RATE = 16000;
    private static final int SAMPLE_DURATION_MS = 500;
    private static final int RECORDING_LENGTH = (int) (SAMPLE_RATE * SAMPLE_DURATION_MS / 1000);
    private static final int PERMISSION_REQUEST_CODE =0 ;
    private static final String LOG_TAG = RecognizeActivity.class.getSimpleName();

    private static String NN_MODEL_FILENAME = "file:///android_asset/frozen_test_server_19.pb";

    private static String OUTPUT_NODE = "dense_3/Sigmoid:0";
    private static String INPUT_NODE = "conv2d_1_input:0";


    private Thread recognitionThread;
    private Thread recordingThread;
    private boolean isRecording = true;
    private boolean isRecognizing = true;
    private boolean save_flag = false;
    boolean spec_flag = false;

    AudioRecord record;
    String outText = "";
    ImageView spectrogramm;
    short[] recordingBuffer = new short[RECORDING_LENGTH];
    int recordingOffset = 0;
    short[] inputBuffer;
    private short[] audioBuffer;
    private float[] outData = new float[2];

    private TensorFlowInferenceInterface tensorFlowInferenceInterface;
    TextView modelOutput;
    ToggleButton toggleButton;
    private final ReentrantLock recordingBufferLock = new ReentrantLock();
    private ToggleButton chawkButton;
    private SoundPool soundPool;
    private AssetManager assetManager;
    private int prepareSound;
    private MediaPlayer player;
    private Thread chawkThread;

    // method will be execute at application start
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_recognize);

        spectrogramm = (ImageView) findViewById(R.id.spectrogram);
        toggleButton = (ToggleButton) findViewById(R.id.toggleButton);
        Button firstModelButton = (Button) findViewById(R.id.firstModel);
        Button secondModelButton = (Button) findViewById(R.id.secondModel);
        Button thirdModelButton = (Button) findViewById(R.id.thirdModel);


        // FIXME changes threads crush app. Need to stop-start thread recognize and change output/input nodes
        firstModelButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NN_MODEL_FILENAME = "file:///android_asset/frozen_test_server_19.pb";
                tensorFlowInferenceInterface = new TensorFlowInferenceInterface(getAssets(), NN_MODEL_FILENAME);

            }
        });
        secondModelButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NN_MODEL_FILENAME = "file:///android_asset/frozen_test_server_18.pb";
                tensorFlowInferenceInterface = new TensorFlowInferenceInterface(getAssets(), NN_MODEL_FILENAME);

            }
        });
        thirdModelButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NN_MODEL_FILENAME = "file:///android_asset/frozen_test_server_17.pb";
                tensorFlowInferenceInterface = new TensorFlowInferenceInterface(getAssets(), NN_MODEL_FILENAME);

            }
        });
        modelOutput = (TextView) findViewById(R.id.testView);

        // load model from asset folder
        NN_MODEL_FILENAME = "file:///android_asset/frozen_test_server_19.pb";
        tensorFlowInferenceInterface = new TensorFlowInferenceInterface(getAssets(), NN_MODEL_FILENAME);

        // onlu for android >5.0
        // load start sound for recording
        AudioAttributes attributes = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_GAME)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build();
        soundPool = new SoundPool.Builder()
                .setAudioAttributes(attributes)
                .build();
        assetManager = getAssets();
        prepareSound = loadSound();
        player = MediaPlayer.create(this, R.raw.alert);

        // request permissions, for example: using microphone, write/read storage and etc.
        requestMultiplePermissions();
    }

    public void requestMultiplePermissions() {
        ActivityCompat.requestPermissions(this,
                new String[] {
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.RECORD_AUDIO
                },
                PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startRecordingThread();
            startRecognitionThread();
        }
    }

    private int loadSound() {
        AssetFileDescriptor afd;
        try {
            afd = assetManager.openFd("thrown.ogg");
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Не могу загрузить файл " + "thrown.ogg",
                    Toast.LENGTH_SHORT).show();
            return -1;
        }
        return soundPool.load(afd, 1);
    }

    private synchronized void startRecordingThread() {
        if (recordingThread != null) {

            return;

        }

        recordingThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    record();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

        });
        recordingThread.start();


    }

    private synchronized void startRecognitionThread() {
        if (recognitionThread != null) {
            return;
        }
        recognitionThread = new Thread(
                new Runnable() {
                    @Override
                    public void run() {

                        recognize();

                    }
                });

        recognitionThread.start();

    }

    private Bitmap getSpectrogram(short[] data){

        final RenderSpectrogram render = new RenderSpectrogram();


        double [] musicDoubles = new double[8000];

        // divide by 33000
        for (int i =0;i<data.length;i++){
            musicDoubles[i] = (double)data[i]/(double)33000;
        }

        STFT stft = new STFT(198,49);


        // stft sample

        final double[][] stftSamples;
        stftSamples = stft.getSTFT(musicDoubles);
        final Bitmap bitmap_spectrogram;
        bitmap_spectrogram = render.renderSpectrogram(stftSamples);


        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                spectrogramm.setImageBitmap(bitmap_spectrogram);
            }
        });


        return bitmap_spectrogram;

    }

    private float[] getFlatNormalizedSpectrogram(Bitmap bitmap){

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int size = bitmap.getRowBytes() * bitmap.getHeight();
        ByteBuffer byteBuffer = ByteBuffer.allocate(size);
        bitmap.copyPixelsToBuffer(byteBuffer);
        int [] formated_spectrogram_data = new int[size];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int color = bitmap.getPixel(i,j);
                int R = Color.red(color);
                int G = Color.green(color);
                int B = Color.blue(color);
                formated_spectrogram_data[i*height+j]= (R+G+B)/3;
            }
        }

        float[] greySpectrogramData = new float[width*height];
        for (int i = 0; i < width*height; i++) {
            greySpectrogramData[i] = (float)(formated_spectrogram_data[i]/255.0);

        }

            return greySpectrogramData;
    }


    // Main method that grab data from microphone
    private void record() throws IOException {

        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_AUDIO);

        // Estimate the buffer size we'll need for this device.
        int bufferSize =
                AudioRecord.getMinBufferSize(
                        SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);

        if (bufferSize == AudioRecord.ERROR || bufferSize == AudioRecord.ERROR_BAD_VALUE) {
            bufferSize = SAMPLE_RATE * 2;
        }

        audioBuffer = new short[bufferSize];

        record = new AudioRecord(MediaRecorder.AudioSource.DEFAULT,
                SAMPLE_RATE,
                AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT,
                bufferSize);

        if (record.getState() != AudioRecord.STATE_INITIALIZED) {
            Log.e(LOG_TAG, "Audio Record can't initialize!");
            return;
        }

        try {
            record.startRecording();

            Log.v(LOG_TAG, "Start recording");
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

        while (isRecording) {


            // Circular buffer to save all data piece by piece equal minBufferSize.
            // [0 0 0 0 0] -> [1 2 0 0 0] -> [1 2 3 4 0]
            int samples = record.read(audioBuffer, 0, audioBuffer.length);
            //  divide by 33000


            int maxLength = recordingBuffer.length;
            int newRecordingOffset = recordingOffset + samples;
            int secondCopyLength = Math.max(0, newRecordingOffset - maxLength);
            int firstCopyLength = samples - secondCopyLength;
            recordingBufferLock.lock();

            // Second System.arraycopy write data to begin of buffer and closed circular buffer
            // audioBuffer = {5,6}
            // [1 2 3 4 0] -> [1 2 3 4 5] (first arraycopy)
            // [1 2 3 4 5] -> [6 2 3 4 5] (second arraycopy)

            try {
                System.arraycopy(audioBuffer, 0, recordingBuffer, recordingOffset, firstCopyLength);
                System.arraycopy(audioBuffer, firstCopyLength, recordingBuffer, 0, secondCopyLength);
                recordingOffset = newRecordingOffset % maxLength;
            } finally {
                recordingBufferLock.unlock();
            }
        }
            record.stop();
            record.release();

    }

    private void recognize() {

        inputBuffer = new short[RECORDING_LENGTH];

        while (isRecognizing) {


            recordingBufferLock.lock();
            try {
                int maxLength = recordingBuffer.length;
                int firstCopyLength = maxLength - recordingOffset;
                int secondCopyLength = recordingOffset;

                // Second System.arraycopy correctly restore data from circular buffer.
                // recodingBuffer = [6 2 3 4 5]
                // [0 0 0 0 0] -> [2 3 4 5 0] (first arraycopy)
                // [2 3 4 5 0] -> [2 3 4 5 6] (second arraycopy)
                System.arraycopy(recordingBuffer, recordingOffset, inputBuffer, 0, firstCopyLength);
                System.arraycopy(recordingBuffer, 0, inputBuffer, firstCopyLength, secondCopyLength);
            } finally {
                recordingBufferLock.unlock();
            }

            try {
                // Feed data to tensorflow model.
                float[] feed_dict = getFlatNormalizedSpectrogram(getSpectrogram(inputBuffer));
                //  add 200 and divide by 400
                for(int i =0; i<feed_dict.length;i++){
                    feed_dict[i] = (feed_dict[i]+200)/(float)(400);
                }
                tensorFlowInferenceInterface.feed(INPUT_NODE, feed_dict,1,33,60,1);
                tensorFlowInferenceInterface.run(new String[] {OUTPUT_NODE});
                tensorFlowInferenceInterface.fetch(OUTPUT_NODE, outData);
                spec_flag = outData[0]>outData[1];
                outText = Boolean.toString(outData[0]>outData[1])+"   "
                        +Float.toString(outData[0])+" " +Float.toString(outData[1]);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        modelOutput.setText(outText);
                    }
                });
                Log.d("info",outText);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }



}

