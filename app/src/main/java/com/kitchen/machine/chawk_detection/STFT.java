package com.kitchen.machine.chawk_detection;

import java.com.google.corp.productivity.specialprojects.android.fft.RealDoubleFFT;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.lang.Math.PI;

import static java.lang.Math.abs;
import static java.lang.Math.cos;
import static java.lang.Math.log10;
import static java.lang.Math.sqrt;


class STFT{


    private int n_fft = 198;
    private int hop_length = 49;
    private int window_size = n_fft;

    STFT(int n_fft, int hop_length){
        this.n_fft = n_fft;
        this.hop_length = hop_length;


    }

    double [] generateHannWindow(){
        double hannWindow[] = new double[window_size];
        for (int i = 0; i<window_size; i++){
            hannWindow[i] = 0.5 * (1 - cos(2 * PI * i / this.n_fft));
        }
        return hannWindow;
    }

    double [][] getWindows(int n_frames, double[] hannWindow) {

        double windows[][] = new double[n_frames][window_size];

        for (int i = 0; i < n_frames; i++) {
            System.arraycopy(hannWindow, 0, windows[i], 0, window_size);
        }
        return windows;
    }

    double [][] frame_data(double[] samples, int n_frames){
        double framing_data[][] = new double[164][198];
        for (int i = 0; i < n_frames; i++) {
            double[] subsample = Arrays.copyOfRange(samples, hop_length*i, 198+(hop_length*i));
            System.arraycopy(subsample, 0, framing_data[i], 0, this.n_fft);
        }
        return framing_data;
    }

    double [] padding(double[] samples){

        List< Double> pad_samples = new ArrayList<Double>();

        for (double sample : samples) {
            pad_samples.add(sample);
        }
        for (int i = 1; i <= 99; i++) {
            pad_samples.add(0, samples[i]);
        }
        for (int i = samples.length-2; i >= samples.length-99; i--) {
           // Object[] test = Arrays.copyOfRange(pad_samples.toArray(), 8090, 8197);
            pad_samples.add(samples[i]);

        }

        double[] padded_samples = new double[samples.length+199];
        for (int i = 0; i < pad_samples.size(); i++) {
            padded_samples[i] = pad_samples.get(i);
        }
        return padded_samples;
    }

    double [][] multiply(double[][] frames, double[][] windows){
        double[][] result = new double[frames.length][windows[0].length];
        for (int i =0; i<frames.length;i++){
            for (int j= 0; j<windows[0].length;j++){
                result[i][j] = frames[i][j]*windows[i][j];
            }
        }
        return result;

    }

    double [][] fft_frames(double[][] windowed_framed_samples){
        double[][] stft_samples = new double[windowed_framed_samples.length]
                [windowed_framed_samples[0].length];
        RealDoubleFFT fft = new RealDoubleFFT(198);

        for(int i =0; i<windowed_framed_samples.length;i++){
            double[] temp = windowed_framed_samples[i];
            fft.ft(temp);
            stft_samples[i] = temp;
        }
        return stft_samples;
    }

    double [][] coeffs_to_amplitude(double[][] stft_samples){
        double[][] amplitude = new double[stft_samples.length][stft_samples[0].length/2+1];

        for(int i =0;i<stft_samples.length;i++){
            List<Double> temp = new ArrayList<>();

            for(int j = 1; j<stft_samples[0].length-1;j+=2){
                temp.add(sqrt(stft_samples[i][j]* stft_samples[i][j]+ stft_samples[i][j+1]*stft_samples[i][j+1]));
            }
            double tempotemp []= new double[stft_samples[0].length/2+1];
            tempotemp[0] = stft_samples[i][0];
            tempotemp[stft_samples[0].length/2] = abs(stft_samples[i][stft_samples[0].length-1]);
            for (int j =0;j<temp.size();j++){
                tempotemp[j+1] = temp.get(j);
            }
            amplitude[i] = tempotemp;
        }
        return  amplitude;
    }

    double [][] transpose(double[][] stft_samples){
        double[][] transpose_samples = new double[stft_samples[0].length][stft_samples.length];
        for (int i =0; i<stft_samples[0].length;i++){
            for (int j= 0;j<stft_samples.length  ;j++){
                transpose_samples[i][j] = stft_samples[j][i];
            }
        }
        return  transpose_samples;
    }

    double [][] ampToDb(double[][] amplitude){
        double[][] db_samples = new double [amplitude.length][amplitude[0].length];
        for (int i =0; i<amplitude.length;i++){
            for (int j= 0;j<amplitude[0].length  ;j++){
                db_samples[i][j] = 10*log10(amplitude[i][j]*amplitude[i][j]);
                if (db_samples[i][j]< -3.2794952){
                    db_samples[i][j] = -3.2794952;
                    }
            }
        }

        return  db_samples;
    }
    double [][] correctDB(double[][] db_samples){
        double db_max = -10000000.0;
        for (int i =0; i<db_samples.length;i++){
            for (int j= 0;j<db_samples[0].length  ;j++){
                if(db_samples[i][j] > db_max){
                    db_max = db_samples[i][j];

                }
            }
        }
        double log_spec =  db_max - 80;

        for (int i =0; i<db_samples.length;i++){
            for (int j= 0;j<db_samples[0].length  ;j++){
                if(db_samples[i][j] < log_spec){
                    db_samples[i][j] = log_spec;
                }
            }
        }

        return  db_samples;
    }

    private double [][] resize(double[][] big_picture, int new_width, int new_height){
        int old_width = big_picture.length;
        int old_height = big_picture[0].length;
        double[][] temp = new double[new_width][new_height] ;
        // EDIT: added +1 to account for an early rounding problem

        double x_ratio = old_width/(double)new_width ;
        double y_ratio = old_height/(double)new_height ;
        double px, py ;
        for (int i=0;i<new_height;i++) {
            for (int j=0;j<new_width;j++) {
                px = Math.floor(j*x_ratio) ;
                py = Math.floor(i*y_ratio) ;
                temp[(j)][i] = big_picture[(int) (px)][(int)(py)] ;
            }
        }
        return temp;

    }

    public double[][] getSTFT(double [] soundSamples){
        double[] hannWindow = generateHannWindow();
        double[][] windows = getWindows(164, hannWindow);
        double[] padded_samples = padding(soundSamples);
        double[][] frames = frame_data(padded_samples, 164);
        double[][] w_f_samples = multiply(frames, windows);
        double[][] result = fft_frames(w_f_samples);
        double[][] amplitude = coeffs_to_amplitude(result);
        final double[][] db_samples = ampToDb(amplitude);
        final double[][] tranposed_db_samples = transpose(db_samples);
        double[][] resized_samples = resize(tranposed_db_samples,33, 60 );
        return resize(correctDB(tranposed_db_samples),33, 60 );
    }
}